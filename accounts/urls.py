from django.urls import path
from accounts.views import Login, user_logout, Signup

urlpatterns = [
    path("signup/", Signup, name="signup"),
    path("logout/", user_logout, name="logout"),
    path("login/", Login, name="login"),
]
